%dw 2.0
output application/json
var code = error.errorType.namespace ++ ":" ++ error.errorType.identifier
---
(Mule::p(code) default Mule::p('MULE:UNKNOWN')) as Number